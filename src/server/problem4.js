const deliveries = require ('./../Public/output/deliveries.json');
const fs = require ('fs');

const result = deliveries.map (key => {
    return {
        "batsman" : key.batsman,
        "dismissed" : key.player_dismissed,
    }
});

console.log (result);

let output = {};

result.forEach (key => {
    if (output.hasOwnProperty (key.batsman)) {
        if (key.dismissed !== '') {
            output[key.batsman]++;
        }
    } else {
        if (key.dismissed !== '') {
            output[key.batsman] = 1;
        }
    }
})

console.log (output);

fs.writeFileSync ('./../Public/output/problem4.json', JSON.stringify (output), "utf-8", (err) => {
    if (err) {
        console.log (err);
    }
});