const matchesperyear = require('./../Public/output/matchesperyear.json');
const fs = require ('fs');

let season = {};

for (let index = 0; index < matchesperyear.length; index++) {
    if (!season.hasOwnProperty (matchesperyear[index].season)) {
        season[matchesperyear[index].season] = matchesperyear[index].season;
    }
}

let playerWon = {};


for (let prop in season) {
    let output = {};
    for (let index = 0; index < matchesperyear.length; index++) {
        if (matchesperyear[index].season === prop) {
            if (output.hasOwnProperty(matchesperyear[index].player_of_match)) {
                output[matchesperyear[index].player_of_match]++;
            }else {
                output[matchesperyear[index].player_of_match] = 1;
            }
        }
    }

    let allPlayer = [];

    for (let prop in output) {
        allPlayer.push ([prop, output[prop]]);
    }

    allPlayer.sort (function(a, b) {
        return b[1] - a[1];
    });

    playerWon[allPlayer[0][0]] = allPlayer[0][1];

    // ans.push (output);
    // console.log (prop);
    // console.log (output);
}

console.log (playerWon);

fs.writeFileSync ('./../Public/output/problem2.json', JSON.stringify (playerWon), "utf-8", (err)=> {
    if (err) {
        console.log (err);
    }
})