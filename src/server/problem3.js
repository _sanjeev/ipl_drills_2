const deliveries = require('./../Public/output/deliveries.json');
const matchPerYear = require('./../Public/output/matchesperyear.json');
const fs = require('fs');

const output = deliveries.map((key, index) => {
    return {
        "name": key.batsman,
        "total_runs": key.total_runs,
        "total_balls": (parseInt(key.over) * 6 + parseInt(key.ball)).toString(),
    }
});

const strike_rate = ((res, name) => {
    let totalRun = 0;

    res.forEach(key => {
        if (key.name === name) {
            totalRun = totalRun + parseInt(key.total_runs);
        }
    });

    // console.log(totalRun);

    let totalBalls = 0;

    res.forEach(key => {
        if (key.name === name) {
            totalBalls = totalBalls + parseInt(key.total_balls);
        }
    });

    // console.log(totalBalls);

    let strike = ((totalRun * 100) / totalBalls);
    let ans = {};
    if (!ans.hasOwnProperty(name)) {
        ans[name] = strike.toFixed(3);
    }
    return ans;
});



let batsman = [];

deliveries.forEach((data, index) => {
    if (!batsman.includes(data.batsman)) {
        batsman.push(data.batsman);
    }
})

let strikeRate = [];

batsman.forEach(name => {
    let ans = strike_rate(output, name);
    strikeRate.push (ans);
})

console.log (strikeRate);

fs.writeFileSync ('./../Public/output/problem3.json', JSON.stringify (strikeRate), "utf-8", (err) => {
    if (err) {
        console.log (err);
    }
});
