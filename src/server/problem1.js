const matchesperyear = require ('./../Public/output/matchesperyear.json');
const fs = require ('fs');

let result = {};

for (let index = 0; index < matchesperyear.length; index++) {
    if (matchesperyear[index].toss_winner === matchesperyear[index].winner) {
        if (result.hasOwnProperty (matchesperyear[index].winner)) {
            result[matchesperyear[index].winner]++;
        } else {
            result[matchesperyear[index].winner] = 1;
        }
    } 
}

console.log (result); 

fs.writeFileSync('./../Public/output/problem1.json', JSON.stringify (result), "utf-8", (err) => {
    if (err) {
        console.log (err);
    }
});