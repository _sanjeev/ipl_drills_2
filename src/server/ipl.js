const csv = require ('csvtojson');
const fs = require ('fs');

const filePath = './../data/deliveries.csv';


csv().fromFile(filePath).then((jsonobject) => {
    console.log (jsonobject);

    fs.writeFileSync('./../Public/output/deliveries.json', JSON.stringify (jsonobject), "utf-8", (err) => {
        if (err) {
            console.log (err);
        }
    })

});