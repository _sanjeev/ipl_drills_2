const deliveries = require ('./../Public/output/deliveries.json');
const fs = require ('fs');

const result = deliveries.map (key => {
    if (key.is_super_over != '') {
        return {
            "over" : (parseInt(key.over) + (parseInt(key.ball) / 6)).toFixed(3).toString(),
            "total_runs" : key.total_runs,
            "bowler" : key.bowler,
        }
    }
});

let output = {};

result.forEach (key => {
    if (output.hasOwnProperty (key.bowler)) {
        output[key.bowler] += (parseFloat (key.total_runs) / parseFloat (key.over));
    } else {
        output[key.bowler] = (parseFloat (key.total_runs) / parseFloat (key.over));
    }
})

fs.writeFileSync ('./../Public/output/problem5.json', JSON.stringify (output), "utf-8", (err) => {
    if (err) {
        console.log (err);
    }
})